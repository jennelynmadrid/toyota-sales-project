import Vue from 'vue'
import Router from 'vue-router'
import Cars from '@/components/Cars'
import Promo from '@/components/Promo'
import Application from '@/components/Application'
import Quote from '@/components/Quote'
import ContactUs from '@/components/ContactUs'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'cars',
      component: Cars
    },
    {
      path: '/cars',
      name: 'cars',
      component: Cars
    },
    {
      path: '/promo',
      name: 'promo',
      component: Promo
    },
    {
      path: '/application',
      name: 'application',
      component: Application
    },
    {
      path: '/quote',
      name: 'quote',
      component: Quote
    },
    {
      path: '/contact_us',
      name: 'contact_us',
      component: ContactUs
    }
  ]
})
